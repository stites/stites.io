{
  description = "stites.io flake";

  inputs.flake-parts.url = "github:hercules-ci/flake-parts";
  inputs.devshell.url = "github:numtide/devshell";
  outputs = { self, nixpkgs, ... }@inputs:
    inputs.flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [ inputs.devshell.flakeModule ];
      systems = [ "x86_64-linux" ];
      perSystem = { config, system, pkgs, ... }: {
        packages.default = pkgs.stdenv.mkDerivation {
          name = "my-homepage";
          src = ./.;
          nativeBuildInputs = with pkgs; [ hugo ];
          buildPhase = "hugo";
          installPhase = "cp -r public $out";
        };
        devshells.default.commands = with pkgs; [{package = hugo; } { package = go; } {package = nodejs;}];
      };
    };
}
